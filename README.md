## How to run

Go to the root of the application and run,

*yarn*

Once done installing the packages, run

*yarn start*

## What is a flash card application?

- It's an interactive application to learn things that you often forget
- It can be used to memorize faq in interviews/ learn new language etc.
- It follows a spaced repeation pattern to help learner memorize thing pretty easily

## How to use?

- Run the application, an you will get some predefined decks to study from 
- If you want to add new decks, click on the add deck button and you are good to go
- Upon selecting a new deck you will be presented with cards to study from
- Add new cards if you want / or click on study deck to start studying
- Individual card will be shown on the screen.
- The card will only show the front where a specific question will be asked.
- Depending on your guess for the answer of the question you can select whether you did good, ok or poorly
- If you did good, the question won't be asked in next three days
- If you did ok, the same question will be asked in next two days
- Last of all if you did poorly, you will be asked the same question the next day
- If you study all the cards, there is nothing left to study

- The application stores the question/answers in the web browser's storage, persistent backend is not added at this moment