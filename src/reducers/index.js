import { combineReducers } from "redux";
import cards from "./cards";
import decks from "./decks";
import addingDeck from "./addingDeck";
import selectedDeck from "./selectedDeck";
import cardFilter from "./cardFilter";
import showingCardBack from "./showingCardBack";
import isFetchingDecks from "./isFetchingDecks";
import isFetchingCards from "./isFetchingCards";
import errorMessage from "./errorMessage";
import fuzzysearch from "fuzzysearch";

let reducers = combineReducers({
  cards,
  decks,
  addingDeck,
  selectedDeck,
  showingCardBack,
  cardFilter,
  isFetchingDecks,
  isFetchingCards,
  errorMessage
});

export default reducers;

export const getVisibleCards = (cards, filter) =>
  cards.filter(
    card =>
      fuzzysearch(filter, card.front.toLowerCase()) ||
      fuzzysearch(filter, card.back.toLowerCase())
  );

const MS_PER_DAY = 86400000;
export const cardToStudy = (cards, deckId) =>
  cards.filter(
    card =>
      card.deckId === deckId &&
      (!card.lastStudiedOn ||
        (+new Date() - card.lastStudiedOn) / MS_PER_DAY >= card.score)
  )[0];
