import {
  FETCH_DECKS_REQUEST,
  FETCH_DECKS_SUCCESS,
  FETCH_DECKS_FAILURE
} from "../actions";

const errorMessage = (state = null, action) => {
  switch (action.type) {
    case FETCH_DECKS_FAILURE:
      return action.message;
    case FETCH_DECKS_REQUEST:
    case FETCH_DECKS_SUCCESS:
      return null;
    default:
      return state;
  }
};

export default errorMessage;
