import { SHOW_CARD_BACK } from "../actions";

const showingCardBack = (state = false, action) => {
  switch (action.type) {
    case SHOW_CARD_BACK:
      return action.show || false;
    default:
      return state;
  }
};

export default showingCardBack;
