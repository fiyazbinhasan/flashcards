import {
  FETCH_DECKS_REQUEST,
  FETCH_DECKS_SUCCESS,
  FETCH_DECKS_FAILURE
} from "../actions";

const isFetchingDecks = (state = false, action) => {
  switch (action.type) {
    case FETCH_DECKS_REQUEST:
      return true;
    case FETCH_DECKS_SUCCESS:
    case FETCH_DECKS_FAILURE:
      return false;
    default:
      return state;
  }
};

export default isFetchingDecks;
