import { FILTER_CARDS } from "../actions";

const cardFilter = (state = "", action) => {
  switch (action.type) {
    case FILTER_CARDS:
      return action.filter.toLowerCase();
    default:
      return state;
  }
};

export default cardFilter;
