import {
  ADD_CARD_SUCCESS,
  FETCH_CARDS_SUCCESS,
  UPDATE_CARD_SUCCESS,
  DELETE_CARD_SUCCESS
} from "../actions";

const cards = (state = [], action) => {
  switch (action.type) {
    case FETCH_CARDS_SUCCESS:
      return action.response;
    case ADD_CARD_SUCCESS:
      return [...state, action.response];
    case UPDATE_CARD_SUCCESS:
      return state.map(card =>
        card.id === action.response.id
          ? Object.assign({}, card, action.response)
          : card
      );
    case DELETE_CARD_SUCCESS:
      let index = state.findIndex(c => c.id === action.id);
      return [...state.slice(0, index), ...state.slice(index + 1)];
    default:
      return state;
  }
};

export default cards;
