import { FETCH_DECKS_SUCCESS, ADD_DECK_SUCCESS } from "../actions";

const decks = (state = [], action) => {
  switch (action.type) {
    case FETCH_DECKS_SUCCESS:
      return action.response;
    case ADD_DECK_SUCCESS:
      return [...state, action.response];
    default:
      return state;
  }
};

export default decks;
