import { SELECT_DECK } from "../actions";

const selectedDeck = (state = null, action) => {
  switch (action.type) {
    case SELECT_DECK:
      return action.id;
    default:
      return state;
  }
};

export default selectedDeck;
