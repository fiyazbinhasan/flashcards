import { SHOW_ADD_DECK, HIDE_ADD_DECK } from "../actions";

const addingDeck = (state = false, action) => {
  switch (action.type) {
    case SHOW_ADD_DECK:
      return true;
    case HIDE_ADD_DECK:
      return false;
    default:
      return state;
  }
};

export default addingDeck;
