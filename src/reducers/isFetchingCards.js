import {
  FETCH_CARDS_REQUEST,
  FETCH_CARDS_SUCCESS,
  FETCH_CARDS_FAILURE
} from "../actions";

const isFetchingDecks = (state = false, action) => {
  switch (action.type) {
    case FETCH_CARDS_REQUEST:
      return true;
    case FETCH_CARDS_SUCCESS:
    case FETCH_CARDS_FAILURE:
      return false;
    default:
      return state;
  }
};

export default isFetchingDecks;
