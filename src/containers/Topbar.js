import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { filterCards, showAddDeck } from "../actions";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  flex: {
    flex: 1
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  gutter: {
    marginLeft: -12,
    marginRight: 20
  },
  textField: {
    margin: 0,
    width: 200
  }
});

const Topbar = ({ classes, showAddDeck, deckId, filterCards }) => {
  return (
    <AppBar color="primary" position="absolute" className={classes.appBar}>
      <Toolbar>
        <Button
          variant="outlined"
          color="inherit"
          className={classes.gutter}
          onClick={() => showAddDeck()}
        >
          Add Deck
        </Button>
        <Typography variant="title" color="inherit" className={classes.flex}>
          Flash Cards
        </Typography>
        {deckId && (
          <div>
            <Button
              color="inherit"
              variant="outlined"
              component={NavLink}
              to={`/deck/${deckId}/new`}
              className={classes.gutter}
            >
              Add Card
            </Button>
            <Button
              color="inherit"
              variant="outlined"
              component={NavLink}
              to={`/deck/${deckId}/study`}
              className={classes.gutter}
            >
              Study Deck
            </Button>
            <TextField
              id="search"
              placeholder="Search cards"
              type="search"
              className={classes.textField}
              onChange={e => filterCards(e.target.value)}
            />
          </div>
        )}
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = ({ selectedDeck }) => ({
  deckId: selectedDeck
});

const mapDispatchToProps = dispatch => ({
  filterCards: filter => dispatch(filterCards(filter)),
  showAddDeck: () => dispatch(showAddDeck())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(Topbar))
);
