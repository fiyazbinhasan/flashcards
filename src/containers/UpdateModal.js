import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import CardModal from "../components/CardModal";
import { updateCard, deleteCard } from "../actions";

const mapStateToProps = (
  { cards },
  {
    match: {
      params: { cardId }
    }
  }
) => ({
  card: cards.filter(card => card.id === cardId)[0]
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  saveCard: card => {
    dispatch(updateCard(card));
    ownProps.history.goBack();
  },
  deleteCard: id => {
    dispatch(deleteCard(id));
    ownProps.history.goBack();
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CardModal)
);
