import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import CardModal from "../components/CardModal";
import { addCard } from "../actions";

const mapStateToProps = (
  props,
  {
    match: {
      params: { deckId }
    }
  }
) => ({
  card: { deckId }
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  saveCard: card => {
    dispatch(addCard(card));
    ownProps.history.push(`/deck/${card.deckId}`);
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CardModal)
);
