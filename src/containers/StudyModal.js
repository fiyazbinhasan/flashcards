import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { showCardBack, updateCard } from "../actions";
import { withStyles } from "@material-ui/core/styles";
import { cardToStudy } from "../reducers";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Slide from "@material-ui/core/Slide";
import Typography from "@material-ui/core/Typography";

const styles = () => ({
  justifyCenter: {
    justifyContent: "center"
  },
  headerLessContent: {
    marginTop: 36,
    display: "flex",
    justifyContent: "center"
  }
});

function Transition(props) {
  return <Slide direction="left" {...props} />;
}

const StudyModal = ({
  classes,
  showingCardBack,
  deckId,
  card,
  showBack,
  scoreCard
}) => {
  return (
    <Dialog open TransitionComponent={Transition} keepMounted fullWidth>
      <DialogContent className={classes.headerLessContent}>
        <Typography variant="title">
          {card
            ? showingCardBack
              ? card.back
              : card.front
            : "You studied all the cards in this deck!"}
        </Typography>
      </DialogContent>
      <DialogActions>
        {card ? (
          showingCardBack ? (
            <div>
              <Button
                color="primary"
                onClick={() => scoreCard(card.id, Math.max(card.score - 1, 1))}
              >
                Poorly
              </Button>
              <Button
                color="primary"
                onClick={() => scoreCard(card.id, card.score)}
              >
                Okay
              </Button>
              <Button
                color="primary"
                onClick={() => scoreCard(card.id, Math.min(card.score + 1, 3))}
              >
                Great
              </Button>
            </div>
          ) : (
            <div>
              <Button color="primary" component={Link} to={`/deck/${deckId}`}>
                Enough for Today
              </Button>
              <Button color="primary" onClick={() => showBack()}>
                Flip
              </Button>
            </div>
          )
        ) : (
          <Button color="primary" component={Link} to={`/deck/${deckId}`}>
            Nice!
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

StudyModal.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (
  { cards, showingCardBack },
  {
    match: {
      params: { deckId }
    }
  }
) => ({
  deckId,
  showingCardBack,
  card: cardToStudy(cards, deckId)
});

const mapDispatchToProps = dispatch => ({
  showBack: () => dispatch(showCardBack(true)),
  scoreCard: (id, score) => {
    let now = new Date();
    now.setHours(0, 0, 0, 0);
    dispatch(
      updateCard({
        id: id,
        score,
        lastStudiedOn: +now
      })
    );
    dispatch(showCardBack(false));
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(StudyModal))
);
