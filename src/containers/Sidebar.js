import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { addDeck, hideAddDeck, selectDeck, fetchDecks } from "../actions";

import DeckList from "../components/DeckList";

import Drawer from "@material-ui/core/Drawer";
import TextField from "@material-ui/core/TextField";
import LinearProgress from "@material-ui/core/LinearProgress";
import Snackbar from "@material-ui/core/Snackbar";
import Button from "@material-ui/core/Button";

const drawerWidth = 300;

const styles = theme => ({
  drawerPaper: {
    position: "relative",
    width: drawerWidth
  },
  textField: {
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginTop: 0,
    marginBotton: 0
  },
  toolbar: theme.mixins.toolbar,
  button: {
    margin: theme.spacing.unit
  },
  extendedIcon: {
    marginRight: theme.spacing.unit
  },
  progress: {
    margin: theme.spacing.unit * 2
  },
  snackbar: {
    margin: theme.spacing.unit
  }
});

class Sidebar extends Component {
  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    const { fetchDecks } = this.props;
    fetchDecks();
  }

  render() {
    const {
      decks,
      addingDeck,
      classes,
      addDeck,
      hideAddDeck,
      selectDeck,
      isFetchingDecks,
      errorMessage
    } = this.props;

    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className={classes.toolbar} />
        {isFetchingDecks ? (
          <LinearProgress color="secondary" />
        ) : errorMessage ? (
          <Snackbar
            open
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center"
            }}
            message={<span id="message-id">Something went wrong!</span>}
            action={[
              <Button
                key="close"
                color="secondary"
                size="small"
                onClick={() => this.fetchData()}
              >
                Try Again
              </Button>
            ]}
          />
        ) : (
          <DeckList decks={decks} selectDeck={selectDeck} />
        )}

        {addingDeck && (
          <TextField
            className={classes.textField}
            id="name"
            label="Press return when done"
            margin="normal"
            autoFocus
            onKeyPress={e => {
              if (e.which !== 13) return;
              else {
                let name = e.target.value.trim();
                if (!name) return;
                addDeck(name);
                hideAddDeck();
              }
            }}
          />
        )}
      </Drawer>
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  decks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired
    })
  ).isRequired,
  addingDeck: PropTypes.bool.isRequired,
  addDeck: PropTypes.func.isRequired,
  hideAddDeck: PropTypes.func.isRequired
};

const mapStateToProps = ({
  decks,
  addingDeck,
  isFetchingDecks,
  errorMessage
}) => ({
  decks,
  addingDeck,
  isFetchingDecks,
  errorMessage
});

const mapDispatchToProps = dispatch => ({
  addDeck: name => dispatch(addDeck(name)),
  hideAddDeck: () => dispatch(hideAddDeck()),
  selectDeck: id => dispatch(selectDeck(id)),
  fetchDecks: () => dispatch(fetchDecks())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(Sidebar))
);
