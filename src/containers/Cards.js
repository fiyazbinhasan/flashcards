import React, { Component } from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { selectDeck, fetchCards } from "../actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getVisibleCards } from "../reducers";

import Card from "../components/Card";
import AddModal from "../containers/AddModal";
import UpdateModal from "../containers/UpdateModal";

import StudyModal from "../containers/StudyModal";

import LinearProgress from "@material-ui/core/LinearProgress";

const styles = theme => ({
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0
  },
  nonPaddedContent: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    minWidth: 0
  },
  cardsContainer: {
    display: "flex",
    flexWrap: "wrap"
  },
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: theme.spacing.unit
  },
  toolbar: theme.mixins.toolbar
});

class Cards extends Component {
  componentDidMount() {
    const { selectDeck, deckId } = this.props;
    selectDeck(deckId);
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    const { deckId } = this.props;
    if (deckId === prevProps.deckId) return;
    else this.fetchData(deckId);
  }

  fetchData() {
    const { fetchCards, deckId } = this.props;
    fetchCards(deckId);
  }

  render() {
    const { classes, cards, isFetchingCards } = this.props;
    if (isFetchingCards)
      return (
        <main className={classes.nonPaddedContent}>
          <div className={classes.toolbar} />
          <LinearProgress color="secondary" />
        </main>
      );

    return (
      <main className={classes.content}>
        <div className={classes.toolbar} />

        <div className={classes.cardsContainer}>
          {cards.map((card, i) => (
            <Card card={card} key={i} />
          ))}
        </div>

        <Route path="/deck/:deckId/new" component={AddModal} />
        <Route path="/deck/:deckId/edit/:cardId" component={UpdateModal} />

        <Route path="/deck/:deckId/study" component={StudyModal} />
      </main>
    );
  }
}

Cards.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (
  { cards, cardFilter, isFetchingCards },
  {
    match: {
      params: { deckId }
    }
  }
) => ({
  cards: getVisibleCards(cards, cardFilter),
  deckId,
  isFetchingCards
});

const mapDispatchToProps = dispatch => ({
  fetchCards: deckId => dispatch(fetchCards(deckId)),
  selectDeck: deckId => dispatch(selectDeck(deckId))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(Cards))
);
