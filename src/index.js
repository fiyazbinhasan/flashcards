import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import registerServiceWorker from "./registerServiceWorker";

import { configureStore } from "./utility/configureStore";
import { saveState } from "./utility/localStorage";
import Root from "./components/Root";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: "#5C6BC0",
      contrastText: "#ffffff"
    },
    secondary: {
      //light: '#0066ff',
      main: "#f48fb1",
      contrastText: "#ffffff"
    }
    //type: 'dark'
  }
});

let store = configureStore();

const render = () => {
  let state = store.getState();
  saveState(state, ["decks", "cards"]);
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <Root store={store} />
    </MuiThemeProvider>,
    document.getElementById("root")
  );
};

registerServiceWorker();
store.subscribe(render);
render();
