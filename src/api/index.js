import { v4 } from "node-uuid";

const fakeDatabase = {
  decks: [
    {
      id: "23db8144-777f-4c4f-9e1d-bc6a5ee9c04a",
      name: "React"
    },
    {
      id: "6e4544fd-5bb4-4285-b32b-e5dd95a9f53e",
      name: "Angular"
    },
    {
      id: "45a020e9-2af8-4165-8611-80e3d7fc048a",
      name: "Blazor"
    }
  ],
  cards: [
    {
      id: v4(),
      front: "What is Redux?",
      back: "A state manager for JS apps",
      score: 1,
      deckId: "23db8144-777f-4c4f-9e1d-bc6a5ee9c04a"
    }
  ]
};

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

/*
   Deck apis
*/
export const fetchDecks = () => delay(1000).then(() => fakeDatabase.decks);

export const addDeck = name =>
  delay(500).then(() => {
    const deck = {
      id: v4(),
      name: name
    };
    fakeDatabase.decks.push(deck);
    return deck;
  });

/*
   Card apis
*/
export const fetchCards = deckId =>
  delay(500).then(() => fakeDatabase.cards.filter(c => c.deckId === deckId));

export const addCard = card =>
  delay(500).then(() => {
    const cardToAdd = {
      id: v4(),
      front: card.front,
      back: card.back,
      score: card.score,
      lastStudiedOn: card.lastStudiedOn,
      deckId: card.deckId
    };
    fakeDatabase.cards.push(cardToAdd);
    return cardToAdd;
  });

export const updateCard = card =>
  delay(500).then(() => {
    let cardToUpdate = fakeDatabase.cards.find(c => c.id === card.id);
    cardToUpdate = Object.assign({}, cardToUpdate, card);
    return cardToUpdate;
  });

export const deleteCard = id =>
  delay(500).then(() => {
    let index = fakeDatabase.cards.findIndex(c => c.id === id);
    fakeDatabase.cards.splice(index, 1);
  });
