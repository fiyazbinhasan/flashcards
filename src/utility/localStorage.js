export const loadState = () => {
  try {
    let serializedState = localStorage.getItem("state");
    if (serializedState === null) return undefined;
    return JSON.parse(serializedState);
  } catch (error) {
    return undefined;
  }
};

export const saveState = (state, props) => {
  try {
    let toSave = {};
    props.forEach(prop => {
      toSave[prop] = state[prop];
    });
    let serializedState = JSON.stringify(toSave);
    localStorage.setItem("state", serializedState);
  } catch (error) {
    return undefined;
  }
};
