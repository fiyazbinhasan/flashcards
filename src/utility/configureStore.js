import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import reducers from "../reducers";
// import { loadState } from '../utility/localStorage';

export const configureStore = () => {
  const middlewares = [thunk];
  if (process.env.NODE_ENV !== "production") middlewares.push(createLogger());
  let store = createStore(reducers, applyMiddleware(...middlewares));
  return store;
};
