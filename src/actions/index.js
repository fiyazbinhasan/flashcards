import * as api from "../api";

export const ADD_CARD = "ADD_CARD";
export const ADD_DECK = "ADD_DECK";
export const UPDATE_CARD = "UPDATE_CARD";
export const DELETE_CARD = "DELETE_CARD";
export const SHOW_CARD_BACK = "SHOW_CARD_BACK";
export const FILTER_CARDS = "FILTER_CARDS";
export const SHOW_ADD_DECK = "SHOW_ADD_DECK";
export const HIDE_ADD_DECK = "HIDE_ADD_DECK";
export const SELECT_DECK = "SELECT_DECK";

export const ADD_DECK_SUCCESS = "ADD_DECK_SUCCESS";
export const ADD_CARD_SUCCESS = "ADD_CARD_SUCCESS";
export const UPDATE_CARD_SUCCESS = "UPDATE_CARD_SUCCESS";
export const DELETE_CARD_SUCCESS = "DELETE_CARD_SUCCESS";

export const FETCH_DECKS_REQUEST = "FETCH_DECKS_REQUEST";
export const FETCH_DECKS_SUCCESS = "FETCH_DECKS_SUCCESS";
export const FETCH_DECKS_FAILURE = "FETCH_DECKS_FAILURE";

export const FETCH_CARDS_REQUEST = "FETCH_CARDS_REQUEST";
export const FETCH_CARDS_SUCCESS = "FETCH_CARDS_SUCCESS";
export const FETCH_CARDS_FAILURE = "FETCH_CARDS_FAILURE";

export const showCardBack = show => ({
  type: SHOW_CARD_BACK,
  show
});

export const filterCards = filter => ({
  type: FILTER_CARDS,
  filter
});

export const showAddDeck = () => ({
  type: SHOW_ADD_DECK
});

export const hideAddDeck = () => ({
  type: HIDE_ADD_DECK
});

export const selectDeck = id => ({
  type: SELECT_DECK,
  id
});

/*
  deck thunks
*/
export const addDeck = name => dispatch => {
  api.addDeck(name).then(response => {
    dispatch({
      type: ADD_DECK_SUCCESS,
      response: response
    });
  });
};

export const fetchDecks = () => (dispatch, getState) => {
  if (getState().isFetchingDecks) {
    return Promise.resolve();
  }
  dispatch({
    type: FETCH_DECKS_REQUEST
  });
  api.fetchDecks().then(
    response =>
      dispatch({
        type: FETCH_DECKS_SUCCESS,
        response: response
      }),
    error =>
      dispatch({
        type: FETCH_DECKS_FAILURE,
        message: error.message || "Server error"
      })
  );
};

/*
  card thunks
*/
export const addCard = card => dispatch => {
  api.addCard(card).then(response => {
    dispatch({
      type: ADD_CARD_SUCCESS,
      response: response
    });
  });
};

export const fetchCards = deckId => (dispatch, getState) => {
  if (getState().isFetchingCards) {
    return Promise.resolve();
  }
  dispatch({
    type: FETCH_CARDS_REQUEST
  });
  api.fetchCards(deckId).then(
    response =>
      dispatch({
        type: FETCH_CARDS_SUCCESS,
        response: response
      }),
    error =>
      dispatch({
        type: FETCH_CARDS_FAILURE,
        message: error.message || "Server error"
      })
  );
};

export const updateCard = card => dispatch => {
  api.updateCard(card).then(response => {
    dispatch({
      type: UPDATE_CARD_SUCCESS,
      response: response
    });
  });
};

export const deleteCard = id => dispatch => {
  api.deleteCard(id).then(() => {
    dispatch({
      type: DELETE_CARD_SUCCESS,
      id
    });
  });
};
