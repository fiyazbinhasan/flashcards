import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import Content from "../components/Content";

import Topbar from "../containers/Topbar";
import Sidebar from "../containers/Sidebar";

const styles = () => ({
  root: {
    overflowY: "auto",
    position: "relative",
    display: "flex",
    height: "100vh"
  }
});

const App = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Topbar />
      <Sidebar />
      <Content />
    </div>
  );
};

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);
