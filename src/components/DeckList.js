import React from "react";

import DeckItem, { EmptyDeckTemplate } from "./Deck";

import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";

const DeckList = ({ decks, selectDeck }) => {
  console.log(decks);
  return (
    <List
      component="nav"
      subheader={
        <ListSubheader component="div">
          All Decks ({decks.length})
        </ListSubheader>
      }
    >
      {decks.length === 0 ? (
        <EmptyDeckTemplate message="No decks available" />
      ) : (
        decks.map((deck, i) => (
          <DeckItem key={i} deck={deck} selectDeck={selectDeck} />
        ))
      )}
    </List>
  );
};

export default DeckList;
