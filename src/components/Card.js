import React from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: theme.spacing.unit,
    textDecoration: "none"
  }
});

const Card = ({ card, classes }) => {
  return (
    <Paper
      className={classes.paper}
      elevation={1}
      component={Link}
      to={`/deck/${card.deckId}/edit/${card.id}`}
    >
      <Typography variant="headline" component="h3">
        {card.front}
      </Typography>
    </Paper>
  );
};

Card.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Card);
