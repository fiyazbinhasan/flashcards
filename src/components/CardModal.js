import React, { Component } from "react";
import { Link } from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

class CardModal extends Component {
  state = {
    id: undefined,
    front: "",
    back: "",
    deckId: undefined
  };

  componentDidMount() {
    const { card } = this.props;
    this.setState({
      id: card.id,
      front: card.front,
      back: card.back,
      deckId: card.deckId
    });
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  render() {
    const { deleteCard, saveCard } = this.props;

    return (
      <Dialog open>
        <DialogTitle>{deleteCard ? "Edit" : "New"} Card</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Typically a question goes into the front and an answer in the back
          </DialogContentText>
          <TextField
            value={this.state.front}
            onChange={this.handleChange("front")}
            multiline
            rows="4"
            margin="normal"
            label="Front"
            fullWidth
          />
          <TextField
            value={this.state.back}
            onChange={this.handleChange("back")}
            margin="normal"
            multiline
            rows="4"
            label="Back"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          {deleteCard ? (
            <Button color="primary" onClick={() => deleteCard(this.state.id)}>
              Delete
            </Button>
          ) : null}
          <Button
            color="primary"
            component={Link}
            to={`/deck/${this.state.deckId && this.state.deckId}`}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            onClick={() =>
              saveCard(
                Object.assign({}, this.state, {
                  front: this.state.front,
                  back: this.state.back
                })
              )
            }
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default CardModal;
