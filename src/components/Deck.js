import React from "react";
import { NavLink } from "react-router-dom";

import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PlaylistPlay from "@material-ui/icons/PlaylistPlay";
import InfoIcon from "@material-ui/icons/Info";

export const EmptyDeckTemplate = ({ message }) => {
  return (
    <ListItem>
      <ListItemIcon>
        <InfoIcon color="primary" />
      </ListItemIcon>
      <ListItemText primary={message} />
    </ListItem>
  );
};

const Deck = ({ deck, selectDeck }) => {
  return (
    <ListItem
      component={NavLink}
      to={`/deck/${deck.id}`}
      button
      onClick={() => selectDeck(deck.id)}
      activeStyle={{ backgroundColor: "#ede7f6" }}
    >
      <ListItemIcon>
        <PlaylistPlay color="primary" />
      </ListItemIcon>
      <ListItemText primary={deck.name} />
    </ListItem>
  );
};

export default Deck;
