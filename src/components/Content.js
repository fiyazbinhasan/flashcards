import React from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Cards from "../containers/Cards";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({
  root: {
    display: "flex",
    flexGrow: 1
  },
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0
  },
  toolbar: theme.mixins.toolbar
});

const Content = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Route path="/deck/:deckId" component={Cards} />
      <Route
        exact
        path="/"
        render={() => (
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Paper className={classes.paper} elevation={1}>
              <Typography variant="headline" component="h3">
                Select a deck to study or start adding cards
              </Typography>
              <Typography component="p">
                Don't have a deck? Click the button on the top left of the
                toolbar to add one.
              </Typography>
            </Paper>
          </main>
        )}
      />
    </div>
  );
};

Content.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Content);
